package com.example.philip.ass11;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Philip on 2015-02-03.
 */
public class SecondFragment extends Fragment {
    private ArrayList<Integer> images;

    GridView listView;
    LinearLayout layout;
    private ImageAdapter adapter;

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            listView = (GridView) getActivity().findViewById(R.id.gridview);
            layout = (LinearLayout) getActivity().findViewById(R.id.progressbar_view);

            GridView gridview = (GridView) getActivity().findViewById(R.id.gridview);
            images = new ArrayList<Integer>();
            adapter = new ImageAdapter(getActivity().getApplicationContext(), images);
            gridview.setAdapter(adapter);

            new Task().execute();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.simple_grid, container, false);

            return rootView;
        }

    class Task extends AsyncTask<String, Integer, Boolean> {
        @Override
        protected void onPreExecute() {
            layout.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            layout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            for (int i = 0; i < 50; i++) {
                images.add(R.drawable.ic_launcher);
            }

            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}


