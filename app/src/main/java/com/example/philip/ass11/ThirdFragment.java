package com.example.philip.ass11;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Philip on 2015-02-03.
 */
public class ThirdFragment extends Fragment {

    private Spinner spinner1, spinner2;
    private Button btnSubmit;
    private CheckBox showSurprise;
    private SeekBar sleepTimeSeekBar;
    private TextView sleepText;
    private int sleepTime;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showSurprise = (CheckBox) getActivity().findViewById(R.id.showSurprise);
        sleepTimeSeekBar = (SeekBar) getActivity().findViewById(R.id.sleepTime);
        sleepText = (TextView) getActivity().findViewById(R.id.sleepText);
        sleepTimeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                sleepTime = progresValue;
                return;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                return;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sleepText.setText("Sleeping: " + sleepTime + " seconds before surprise.");
                Toast.makeText(getActivity().getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();
            }
        });

        addListenerOnButton();
        addListenerOnSpinnerItemSelection();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.spinner, container, false);

        return rootView;
    }


    public void addListenerOnSpinnerItemSelection() {
        spinner1 = (Spinner) getActivity().findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                return;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });
    }

    // get the selected dropdown list value
    public void addListenerOnButton() {

        spinner1 = (Spinner) getActivity().findViewById(R.id.spinner1);
        btnSubmit = (Button) getActivity().findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create(); //Read Update
                alertDialog.setTitle("Surprise surprise!! You've selected:");
                alertDialog.setMessage("" + spinner1.getSelectedItem());

                alertDialog.setButton("Continue..", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // here you can add functions
                    }
                });
                if (showSurprise.isChecked()) {
                    try {
                        Thread.sleep(sleepTime * 1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    alertDialog.show();
                }

            }

        });
    }
}
